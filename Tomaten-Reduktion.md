# Grosi's Cherry Tomaten Reduktion

### Zubereitung:
1.	Cherry Tomnaten (Mexikanische "Cherry Honey") halbieren und mit Schale nach unten im Ofen bei 80-90° über 3-4 Stunden reduzieren.

2.	Resultat durch feines Sieb drücken sodass nur Kerne und Häute übrig bleiben.

3.	Nach Geschmack etwas Salz beigeben.

4.	Direkt verwenden oder für später einfrieren.

5.	Weitere Zutaten wie Basilikum, Knoblauch o.ä. erst kurz vor der Verwendung beigeben.

### Mögliche Speisen:
1.	**Bruschetta:** Mit Olivenöl abgebratenes Brot in mundgrossen Stücken (oder toasten und mit etwas Olivenöl besprayen). Mit Hausgemachtzer Knoblauchbutter + Tomaten Reduktion bestreichen.

2.	