# Mutti's Filetgulasch Stroganoff

##### _Für 4 Personen_


### Zutaten:
|||
|------:|:------|
|600g 	|	Rindshuft (Alternativ: Rindsfiletspitz) als ca. 1.5cm grosse Würfel/Streifen |
|1 		|	Zwiebel|
|1 		|	Knoblauchzehe|
|1 		|	rote Peperoni|
|2 		|	Essiggurken|
|3dl	|	Halbrahm|
|2dl 	|	guter Rotwein|
|1El 	|	Mehl|
|1El 	|	Tomatenpüree|
|2Tl 	|	Rindsbouillonpaste/Bratenjus|
|3Tl	|	Paprika|
|etwas 	|	Pfeffer + Salz|
| 		|	Brat-Butter o.Ä.|



### Zubereitung:

**Vorbereiten:** Ofen auf 60° vorheizen, Platte und Teller vorwärmen.

|||
|:------|:------|
|600g Rindshuft 						|	vom Metzger in ca. 1.5cm grosse Würfel oder Streifen schneiden lassen|
|Bratbutter								|	Fleisch portionenweise in der Bratpfanne kurz braten, mit|
|3Tl Paprika + etwas Pfeffer			|	würzen, in Suppenteller im Ofen bei 60° warmstellen|
|1 Zwiebel, 1 Knoblauchzehe gehackt 	|	in Bratbutter golden andämpfen|
|1El Tomatenpüree, 1El Mehl			|	mitdämpfen, mit|
|2dl gutem Rotwein						|	ablöschen|
|1 rote Peperoni, in Streifen 			|	zugeben, etwas einkochen lassen|
|2Tl Rindsbouillonpaste/Bratenjus 		|	dazugeben|
|2 Essiggurken in Streifen 				|	1 Min. mitkochen|
|3dl Halbrahm 							|	dazugiessen, Fleisch wieder beigeben heiss werden lassen, nicht kochen, Sauce mit|
|Salz und Pfeffer 						|	abschmecken|

**_Zu ca. 300g Reis oder Nudeln servieren_**
