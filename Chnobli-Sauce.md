# Mutti's Chnobli-Sauce

### Zutaten:
|||
|------:|:------|
|1/2 l 			|	Rahm (halb oder voll)|
|ca. 1/4 Tube 	|	Mayonnaise (nicht zu viel)|
|4-5 Zehen 		|	Knoblauch (fein geraffelt)|
|ca. 30 g 		|	Peterli (fein gehackt)|
|etwas Essig 	|	Weisser Balsamico Essig und/oder aus Essiggurken-Glas|


### Zubereitung:
|||
|:------|:------|
|Etwas Balsamico mit Mayo mischen|
|+ Knoblauch fein raffeln und beigeben|
|+ Peterli fein hacken und beigeben|
|Rahm steif schlagen und Mayo-Mischung darunterziehen|
|Kaltstellen und nach 30min mit extra Knoblauch und/oder Gurkenessig abschmecken|