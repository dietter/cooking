# Grosi's SuperDuper-Tee - № 1

### Zutaten:
|||
|------:|:------|
|3?? l 				| 	Wasser|
|40?? g				|	Frische Apfel-Minze Blätter*|
|20?? g				|	Frische Zitronen-Verveine (Verbene/Wohlriechendes Eisenkraut) Blätter*|
|etwas				|	Frische Zitronen-Melisse Blätter*|
|ca. 22 cl (+7.5%) 	| 	**Selbstgemachter** Holunderblüten-Sirup (3:4 Sirup => 2000g/1500ml => 13.333g/cl)|
|?? g 				| 	Zucker (Agavendicksaft?)|

\* Am Morgen pflücken und gleich verarbeiten.

_**Ergibt ca. 5.22 Liter**_




### Zubereitung:
1.	Wasser aufkochen.

2.	Blätter beigeben (ca. 2-3 min ziehen lassen).

3.	Absieben oder Sud durch Passiertuch in Flaschen füllen, abkühlen lassen und dann kühl stellen.

4.	Nach bedarf mit **selbstgemachtem** Holunderblüten-Sirup süssen.