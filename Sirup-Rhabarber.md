# Rhabarber-Sirup V1

### Zutaten (für ca. 2.5l Sirup):
|||
|------:|:------|
|2 kg		|	Zucker|
|1 l		|	Rhabarber-Saft|
|0.5 l		|	Wasser|
|15 g		|	Zitronensäure (Apotheke)|
|1 			|	Passiertuch|

### Zubereitung:
1.	Zucker und Zitronensäure in grosse, nicht metallische Schüssel geben, mischen

2.	Wasser mit Rhabarber-Saft mischen und aufkochen

4.	Sud durch Passiertuch in Schüssel geben und Zucker durch Rühren auflösen

5.	In Flaschen füllen, mit Gazetuch und Gummiband verschliessen und abkühlen lassen