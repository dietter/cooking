# Bergamotte Curd

**Von:** https://zartbitter-und-zuckersuess.de/bergamotte-curd/

_Dieses Rezept basiert auf der Zitronartigen Bergamotte (ital.) mit nur sehr dünner Schale._

### Zutaten für 5 Gläser à 220 ml Bergamotte Curd:
|||
|------:|:------|
|200 ml 	|	Bergamotte-Saft (Eine Frucht ergibt ca. 70 – 90 ml Saft.)|
|6 TL 		|	Bergamotte-Abrieb Bioqualität|
|300 g 		|	Zucker|
|200 g 		|	Butter|
|1 Prise 	|	Salz|
|10 		|	Eigelbe|

### Zutaten für 2 Gläser à 220 ml Bergamotte Curd:
|||
|------:|:------|
|80 ml 		|	Bergamotte-Saft (Eine Frucht ergibt ca. 70 – 90 ml Saft.)|
|2.4 TL 	|	Bergamotte-Abrieb Bioqualität|
|120 g 		|	Zucker|
|80 g 		|	Butter|
|0.4 Prise 	|	Salz|
|4 			|	Eigelbe|



### Zubereitung:

**Vorbereiten:** Eier etwas vorher aus dem Kühlschrank nehmen und auf Raumtemperatur bringen.

1.	Die Bergamotten waschen.  
	Anschliessend die Bergamotten ein paar Mal kräftig hin- und herrollen, so kannst Du mehr Saft gewinnen.
	
2.	Mit der Micorplane Bergamotte-Abrieb herstellen, mit Zucker mischen und abgedeckt zur Seite stellen.  
	
	> "Die abgeriebene Schale mit dem Zucker richtig gut vermischen und damit meine ich nicht einfach zusammenrühren, sondern Du solltest die Zutaten mit den Händen richtig miteinander vermischen. Das macht nicht nur total weiche Hände, es sorgt für ein mega intensives Aroma."
	
	_Unklar ob das was bringt oder nur Verschwendung in Form von öligen Händen ist._  
	**Alternativ:** Zucker und Abrieb kurz vakumieren (Oleo Saccharum).
	
3.	Die abgeriebenen Bergamotten halbieren und mit einer Zitruspresse auspressen (Eine Frucht ergibt ca. 70 – 90 ml Saft).
	
4.	Butter in einem Wasserbad schmelzen.  
	Bergamottezucker, Salz und den gesiebten Bergamottsaft beigeben und rühren bis sich der Zucker komplett aufgelöst hat.

5.	Die Eigelbe gut verrühren und beigeben (beide Flüssigkeiten sollten möglichst die selbe Temperator haben).

6.	Jetzt wird alles über dem heissen Wasserbad erwärmt, dabei solltest Du ständig rühren, bis sich die Konsistenz verändert und andickt. Die Curd-Masse läuft jetzt nur noch schwerfällig und puddingartig über den Rücken des Kochlöffels.  
	Für diesen Vorgang, der sich auch „Zur Rose kochen“ nennt, kannst du auch ein Thermometer verwenden und bei genau **80 Grad** kannst Du den Topf schnell vom Herd nehmen. Verpasst Du diesen Moment, gibt’s Bergamotte Rührei.

_Manchmal passiert es, dass das Eigelb ausflockt. Passiert das, kannst Du alles noch durch ein Sieb streichen bevor du es in die sterilisierten Gläser abfüllst._

-----

[![1](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd5-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd5.jpg)
[![2](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd12-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd12.jpg)
[![3](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd15-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd15.jpg)
[![4](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd16-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd16.jpg)
[![5](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd29-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd29.jpg)
[![6](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd20-300x200.jpg)](https://zartbitter-und-zuckersuess.de/wp-content/uploads/2018/02/Bergamotte_Curd20.jpg)
