### Pasteurisieren:
1.	Gläser/Flaschen im Wasserbad (bis zur Hälfte der Gläser/Flaschen) langsam auf über 72 °C erwärmen

2.	Flüssigkeit erwärmen \*\*\*

3.	Deckel/Bügel/Gummis sterilisieren (kurz abkochen und/oder mit Alkohol (70%) reinigen)

4.	Ränder der Gläser/Flaschen mir Alkohol (70%) abwischen

5.	Heiss bis zum Rand in Gläser/Flaschen füllen

6.	Kontrollieren dass Temperatur >72 °C - anderenfalls im Wasserbad nochmals erwärmen

6.	Deckel/Bügel verschliessen sodass keine Luft ins Gefäss gelangt

7.	Auf Holzbrett o.ä. abstellen und langsam ablkühlen lassen (ohne Zugluft; eventuell mit Tuch abdewcken)

-----

|\*\*\* Flüssigkeit			|Erwärmen auf|
|:------|:------|
|Sirup						|72 °C|
|Tee						|72?? °C|
|Tomaten					|80 °C|
|Citrus Saft				|?? °C|
|Konfitüre, Gelée (??, ??)	|?? °C|
|Konfitüre, Gelée (??, ??)	|?? °C|