# Mutti's Gugelhopf

##### _Tassenrezept, Tasse = 3dl ≈ 200g Mehl ≈ 300g Zucker_

### Zutaten:
|||
|------:|:------|
|1 Tasse (3dl) 			|	Rahm|
|1 Tasse (ca. 300g) 	|	Zucker|
|2 						|	Eier (Eigelb und Eiweiss trennen)|
|2 Tassen (ca. 400g) 	|	Mehl|
|1 Btl. (15g) 			|	Backpulver|
|1 						|	Zitronenabrieb|
|1 Gutsch 				|	Milch|
| 						|	Rosinen oder Schokostücke|



### Zubereitung:
|||
|:------|:------|
|Rahm + Zucker + Eigelb 						|	hellrühren|
|Mehl + Backpulver + Zitronenabrieb 			|	dazu rühren|
|1 Gutsch Milch + Rosinen oder Schokostücke 	|	dazu rühren|
|Eischnee 										|	schlagen und darunterheben|



**_In die Form geben und auf der untersten Rille 50Min. bei 170° mit Heissluft (sonst 180°) backen._**
