# Mah-Mee

##### _Für 4 Personen_

### Zutaten:
|||
|------:|:------|
|400g 	|	**Pouletbrüstli**, Kalbfleisch und/oder Schweinefleisch|
|25g 	|	Mah-Mee Gewürz|
|1 		|	rote Peperoni|
|1/2     |	Kopfsalat|
|400g 	|	Chinesische Nudeln|
|60g 	|	Schinken (Aufschnitt)|
|1 		|	Omelette (Flädli)|
|etwas 	|	Butter|
|etwas 	|	Soja-Sauce|
||**OPTIONAL**|
|40g     |	getrocknete Steinpilze (vorher kurz eingeweicht)|
|etwas 	|	Poulet Leber|




### Zubereitung (Originalrezept nach Sepp Kopp):
||
|:------|
|400g Pouletbrüstli, Kalbfleisch und/oder Schweinefleisch in ca. 1 cm grosse Würfel schneiden.|
|In einer Schüssel leicht salzen, mit 3-4 Teelöffel Mah-Mee-Gewürz bestäuben, gut mischen und in heissem Pflanzenfett kurz anbraten.|
|Eine Peperoni, einen halben Kopfsalat und ca. 40g getrocknete Steinpilze (vorher kurz eingeweicht) in feine Julienne geschnitten 2 bis 3 Minuten in Butter dämpfen.|
|400g abgekochte chinesische Nudeln mit dem Fleisch und dem Gemüse in eine Pfanne geben, mit Mah-MeeGewürz reichlich bestäuben, mit Soja-Sauce abschmecken, alles gut gemischt auf einer Platte anrichten und heiss stellen.|
|60g Schinken und eine Omelette in dünne Streifen schneiden, mit Mah-Mee-Gewürz bestäuben, in heisser Butter erhitzen und über das fertige Gericht gut verteilen.|

_En Guete._


**_50g Mah-Mee-Gewürz reichen für ca. 8 – 12 Portionen._**