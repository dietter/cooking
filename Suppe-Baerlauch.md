# Grosi's Bärlauch Suppe

##### _Ergibt ca. 4-6 Portionen_


### Zutaten:
|||
|------:|:------|
|20g	|	Butter|
|20g	|	Mehl|
|1dl	|	Weisswein|
|4dl	|	Geflügelfond oder Hühnerbouillon|
|2dl	|	Halbrahm|
|50g	|	Bärlauchblätter|
|1dl	|	Rahm|

### Zubereitung:
1.	In der Pfanne die Butter schmelzen und das Mehl darin unter Rühren kurz dünsten.

	Mit dem Weisswein und dem Fond oder der Bouillon ablöschen.

	Auf kleinem Feuer zugedeckt 10min leise kochen lassen..

2.	Den Halbrahm beifügen und die Suppe nochmals durchkochen lassen.

3.	Inzwischen die Bärlauchblätter waschen.

	Im Salzwasser nur gerade 1/2 min blanchieren, abschütten und sofort kalt abschrecken damit die Farbe erhalten bleibt.

	Im Cutter oder mit dem Wiegemesser fein hacken.

4.	Den Rahm steif schlagen.

5.	Unmittelbar vor dem Servieren den Bärlauch in die Suppe geben.

	Den Schlagrahm beifügen und die Suppe nach Belieben mit dem Stabmixer kurz aufschlagen.

*****

**_Sofort Servieren!_**