# Rhabarber-Saft

### Zutaten (für ca. 2l Rhabarber-Saft):
|||
|------:|:------|
|1+ kg		|	Rhabarber|
|1.5 l		|	Wasser|
|1			|	Sieb / Salatschleuder|
|1			|	Passiertuch|

### Zubereitung:
1.	Den Rhabarber putzen und in ca. 2 cm lange Stücke schneiden

2.	Mit dem Wasser in einen grossen Topf geben, aufkochen und ca. 15 Minuten köcheln lassen bis der Rhabarber zu Brei zerfallen ist

3.	Das Sieb oder die Salatschleuder mit dem Passiertuch auslegen, die Rhabarbermasse hineingeben und mit einem Löffel druch das Passiertuch drücken oder durch schleudern die Flüssigkeit von der Masse trennen

4.	Den gewonnenen Saft nochmals wenige Minuten auf 72?? °C aufkochen

5.	Heiss in sterilisierte Flaschen geben und sofort verschliessen und langsam abkühlen lassen

**Mit Mineralwasser und Eiswürfeln geniessen (+ ggf. Limette)**

**ODER: für bessere Haltbarkeit und "weniger" Säure zu Rhabarber-Sirup verarbeiten**