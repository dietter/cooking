# Mutti's Schokokuchen

### Zubereitung:
||||
|------:|:------|:------|
|150 gr. 	|	Butter 					|	schmelzen|
|120 gr. 	|	dunkle Schokolade 		|	zerkleinern, beigeben|
||||
|1⁄2 Kl. 	|	Kaffeepulver 			|	in|
|4 El. 		|	heissem Wasser 			|	auflösen, alles mit|
|etwas 		|	Zimt + Nelkenpulver 	|	zu Butter/Schokolade geben und verrühren, abkühlen lassen|
||||
|250 gr. 	|	Zucker||
|6 			|	Eigelb 					|	mischen und hellrühren|
||||
|250 gr. 	|	Mandeln (gerieben)||
|50 gr. 	|	Zwieback (gerieben) 	|	abwechslungsweise von Hand einrühren|
|6 			|	Eischnee 				|	schlagen und sorgfältig darunterziehen|



**_
Backform mit Backtrennpapier auskleiden.
Auf unterster Rille bei 180° gut 1 Std. (Heissluft 170°, 55 Minuten) backen.
Nach dem Abkühlen mit Puderzucker bestäuben.
_**
