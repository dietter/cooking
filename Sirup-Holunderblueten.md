# Grosi's Holunderlüten-Sirup

### Zutaten (für ca. 2.5l Sirup):
|||
|------:|:------|
|1.5 l							|	Wasser|
|2 kg							|	Zucker|
|30 g							|	Zitronensäure (Apotheke)|
|50+ g* (von ca. 20-30 Dolden)	| 	Holunderlüten|
|1 								|	Passiertuch|

**\* Nur die frischen Blüten**


### Zubereitung:
1.	Reife Blüten von Dolden abstreifen (Diese lösen sich fast von selbst)

2.	Welke Blüten und Tierchen entfernen
 
3.	Zucker und Zitronensäure in grosse, nicht metallische Schüssel geben, mischen

4.	Wasser aufkochen

5.	Blüten beigeben (ca. 2-3 min ziehen lassen)

6.	Sud durch Passiertuch in Schüssel geben und Zucker durch Rühren auflösen

7.	In Flaschen füllen, mit Gazetuch und Gummiband verschliessen und abkühlen lassen