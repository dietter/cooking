# Grosi's Goldmelissen-Sirup

### Zutaten (für ca. 2.5l Sirup):
|||
|------:|:------|
|1.5 l		|	Wasser|
|2 kg		|	Zucker|
|30 g		|	Zitronensäure (Apotheke)|
|?? g 		| 	Goldmelissenblüten|
|1 			|	Passiertuch|

### Zubereitung:
1.	Zucker und Zitronensäure in grosse, nicht metallische Schüssel geben, mischen

2.	Wasser aufkochen

3.	Goldmelissenblüten beigeben (ca. 2-3 min ziehen lassen, bis Diese Ihre Farbe verloren haben)

4.	Sud durch Passiertuch in Schüssel geben und Zucker durch Rühren auflösen

5.	In Flaschen füllen, mit Gazetuch und Gummiband verschliessen und abkühlen lassen