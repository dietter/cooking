# Kaffe-Likör

### Zutaten für ca. 830ml:
|||
|------:|:------|
|1 l 		|	Rum|
|100 g 		|	Kaffepulver|
|300 g 		|	Kandiszucker|


### Zubereitung:
1.	Kaffepulver in Rum geben
	
2.	Ca. 3h warten (ab und zu schütteln)
	
3.	Gemisch filtern (Passiertuch o.ä.)
	
4.	Kandiszucker in Flaschen geben und mit Kaffe-Schnaps auffüllen: 120g für 3.3dl Falsche / 180g für 5dl Flasche

5.	Flaschen über die nächsten 2-3 Tage ab und zu schütteln bis sich der Kandiszucker aufgelöst hat.