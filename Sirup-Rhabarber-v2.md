# Rhabarber-Sirup V2

### Zutaten (für ca. 2.5l Sirup):
|||
|------:|:------|
|2 kg		|	Zucker|
|1.5 l		|	Rhabarber-Saft|
|15 g		|	Zitronensäure (Apotheke)|
|1 			|	Passiertuch|

### Zubereitung:
1.	Zucker und Zitronensäure in grosse, nicht metallische Schüssel geben, mischen

2.	Zucker mit Rhabarber-Saft mischen und nochmals auf 72 °C erhitzen

3.	In Flaschen füllen, sofort verschliessen und abkühlen lassen